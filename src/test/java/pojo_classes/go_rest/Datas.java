package pojo_classes.go_rest;

import lombok.Builder;
import lombok.Data;

@Data
@Builder


public class Datas {

    private int id;
    private int post_id;
    private String name;
    private String email;
    private String body;
}
